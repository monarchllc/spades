angular.module('socially').filter('uninvited', function () {
    return function (users, party) {
        if (!party)
            return false;

        //console.log("univited filter", users, party);

        return _.filter(users, function (user) {

            if (user._id == party.owner ||
                _.contains(party.invited, user._id)) {
                console.log("reject this one");
                return false;
            }
            else {
                console.log("accept this one");
                return true;
            }
        });
    }
});