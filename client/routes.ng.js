angular.module("socially").run(function ($rootScope, $state) {
    $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
        // We can catch the error thrown when the $requireUser promise is rejected
        // and redirect the user back to the main page
        if (error === 'AUTH_REQUIRED') {
            $state.go('parties');
        }
    });
});

angular.module('socially').config(function($urlRouterProvider, $stateProvider, $locationProvider){

    $locationProvider.html5Mode(true);

    $stateProvider
        .state('parties', {
            url: '/parties',
            templateUrl: 'client/parties/views/parties-list.ng.html',
            controller: 'PartiesListCtrl'
        })
        .state('scoreboard', {
            url: '/score',
            templateUrl: 'client/scoreKeeper/views/scoreBoard.ng.html',
            controller: 'scoreKeeperCtrl'
        })
        .state('menu', {
            url: '/',
            templateUrl: 'client/mainMenu/mainMenu.ng.html',
            controller: 'MainMenuController'
        })
        .state('mindsOfMonarchs', {
            url: '/MindsOfMonarchs',
            templateUrl: 'client/mainMenu/mainMenu.ng.html',
            controller: 'MindsOfMonarchsCtrl'
        })
        .state('partyDetails', {
            url: '/parties/:partyId',
            templateUrl: 'client/parties/views/party-details.ng.html',
            controller: 'PartyDetailsCtrl',
            resolve: {
                "currentUser": function ($meteor) {
                    return $meteor.requireUser();
                }
            }
        })
        .state('rights', {
            url: '/rights',
            templateUrl: 'client/knowYourRights/views/knowYourRights.ng.html',
            controller: 'KnowYourRightsCtrl'
        })
        .state('blog', {
            url: '/blog',
            templateUrl: 'client/monarchDeveloperBlog/monarchDeveloperBlog.ng.html'
        });

    //$urlRouterProvider.otherwise("/parties");
});