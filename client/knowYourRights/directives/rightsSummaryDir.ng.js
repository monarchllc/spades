angular.module('socially')
    .service('rightsSummary', function(bidService) {
        // this directive should be applied to current Games
        // then create a validateGameResults
        // that directive should validated Game results
        //

        return {
            restrict: 'E',
            scope: {
                law: "="
            },
            templateUrl: 'client/knowYourRights/directives/rightsSummary.ng.html'
        };
    });