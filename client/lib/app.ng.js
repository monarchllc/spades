angular.module('socially',
    ['angular-meteor',
        'ui.router',
        'accounts.ui',
        'angularUtils.directives.dirPagination',
        'uiGmapgoogle-maps',
        'ui.bootstrap',
        'ngMessages'
    ]);

function onReady() {
    angular.bootstrap(document, ['socially'], {
        strictDi: true
    });
}

if (Meteor.isCordova)
    angular.element(document).on("deviceready", onReady);
else
    angular.element(document).ready(onReady);

// These should be config variables
angular.module('socially').value('BID_INITIAL_VALUE', undefined);
angular.module('socially').value('DEFAULT_HI_SCORE', 300);
angular.module('socially').value('DEFAULT_MIN_BID', 4);
angular.module('socially').value('DEFAULT_MAX_BID', 13);

