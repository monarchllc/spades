angular.module('socially').service('SpadesRulesService',
    function (DEFAULT_HI_SCORE, DEFAULT_MIN_BID, DEFAULT_MAX_BID) {
            this.highScore = DEFAULT_HI_SCORE;
            this.minBid = DEFAULT_MIN_BID;
            this.maxBid = DEFAULT_MAX_BID;
    });