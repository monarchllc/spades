angular.module('socially').factory('SpadesTeamFactory',
    function ($log, SpadesRulesService) {
        return function(name){
            if(name) {
                return {
                    name: name,
                    currentBid: SpadesRulesService.minBid,
                    isScoreFinalized: false,
                    runningTotals: [],
                    resetForNextHand: function() {
                        currentBid = SpadesRulesService.minBid;
                            isScoreFinalized = false;
                    },
                    calculatedScores: [],
                    calculateHandScore: function() {
                        var score = this.score - this.currentBid;

                        if(score < 0) {
                            $log.log("Team", this.name, " Bumped...lol");
                            score = this.currentBid * -1;
                        } else {
                            $log.log("Team", this.name, " Got Theirs...lol");
                            score = this.currentBid;
                        }

                        score *= 10;

                        //TODO: create function on SpadesTeam
                        this.calculatedScores.push(score);

                        if(this.calculatedScores.length > 1) {
                            var sum = 0;

                            for(var z = 0; z <= this.calculatedScores.length; z++){
                                if(this.calculatedScores[z]) {
                                    sum += parseInt(this.calculatedScores[z]);
                                    $log.log("adding ", this.calculatedScores[z]);
                                }
                            }


                            this.runningTotals.push(sum);
                            $log.log("calculating sum ", this.runningTotals);
                        }
                        this.currentBid = SpadesRulesService.minBid;
                        this.isScoreFinalized = false;

                        $log.log("The score being pushed is ", score);
                        return score;
                    }
                    // score
                    //
                }
            } else {
                return {
                    error:"Your team must have a name"
                }
            }
        };

    });