angular.module('socially')
    .directive('validateHandScore', function(bidService) {
        // this directive should be applied to current Games
        // then create a validateGameResults
        // that directive should validated Game results
        //

        return {
            restrict: 'A',
            link: function(scope) {
                scope.functionsForValidator = {};
                // TODO: have to add values for error messages to this functionsForValidator
                //
                scope.functionsForValidator.scoreValid = function(modelValue, viewValue) {
                    if (modelValue === undefined || modelValue === "") {
                        // consider empty models to be valid
                        return true;
                    }

                    console.log("scoreValid ", viewValue);

                    if (viewValue <= 13 && viewValue >= 0) {
                        // it is valid
                        return true;
                    }

                    // it is invalid
                    return false;
                };
            }
        };
    });