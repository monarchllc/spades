angular.module('socially')
    .directive('validateCurrentGame', function(bidService) {
        // this directive should be applied to current Games
        // then create a validateGameResults
        // that directive should validated Game results
        //

        return {
            restrict: 'A',
            link: function(scope) {
                scope.functionsForValidator = {};
                scope.functionsForValidator.bidValid = function(modelValue, viewValue) {
                    if (modelValue === undefined || modelValue === "" || modelValue === NaN) {
                        // consider empty models to be valid
                        return true;
                    }

                    if (bidService.isTeamBidValid(viewValue)) {
                        // it is valid
                        return true;
                    }

                    // it is invalid
                    return false;
                };
                console.log("validateCurrentGame", scope);
            }
        };
    });