angular.module('socially').controller('currentGameModalCtrl',
    function ($scope, $modalInstance, bid) {

    $scope.currentBid = bid < 0 ? 0 : bid;

    $scope.ok = function () {
        $modalInstance.close($scope.currentBid);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});