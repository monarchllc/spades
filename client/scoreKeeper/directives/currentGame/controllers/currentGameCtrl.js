angular.module("socially").controller("CurrentGameController", function($scope, $modal, $log, BID_INITIAL_VALUE, $rootScope, bidService) {
    $scope.padding = {};
    $scope.padding.teamBid = $scope.teamBid;

    //$scope.$watch(function(){
    //        return $scope.teamBid;
    //    },
    //    function(newValue) {
    //        if(newValue === BID_INITIAL_VALUE) {
    //            $scope.padding.teamBid = BID_INITIAL_VALUE;
    //        }
    //    });

    console.log("checking that validate currentGame worked", $scope);

    $scope.lockTeamBid = function(bidToLock) {
        console.log("locking team bid from input", bidToLock, $scope.padding.teamBid);
        //use bid service to check bid

        $scope.padding.teamBid =  bidToLock;
    };
    $scope.isTeamScoreSet = false;
    console.log("what's my teams bid ", $scope.padding.teamBid);
    if($scope.padding.teamBid && $scope.padding.teamBid !== BID_INITIAL_VALUE) {
        $scope.isTeamScoreSet = true;
    }

    $scope.$watch(function(){
        return $scope.isScoreFinalized;
    }, function(newValue, oldValue) {
        if(newValue !== oldValue) {
            if(newValue === true) {
                $scope.isTeamScoreSet = false;
            }
        }
    });

    $scope.$watch(function() {
        return $scope.padding.teamBid;
    },function(newValue, oldValue) {
        if(newValue != oldValue) {
            $scope.teamBid = newValue;
            console.log("*teamBid has changed in current game", newValue);
            if(newValue != BID_INITIAL_VALUE) {
                $scope.isTeamScoreSet = true;
            } else {
                $scope.isTeamScoreSet = false;
            }

            $scope.currentGameBidsForm[$scope.teamName+'-bidInput'].$setTouched();
        }
    },true);
});