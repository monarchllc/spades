angular.module('socially')
    .directive('currentGame', function() {
    return {
        restrict: 'E',
        scope: {
            teamBid: '=',
            teamName: '=',
            isScoreFinalized: '=',
            currentGameBidsForm: '=',
            placeholderText: '='
        },
        link: function(scope) {
            if(scope.$parent.functionsForValidator) {
                scope.functionsForValidator = scope.$parent.functionsForValidator;

                if(scope.functionsForValidator && scope.functionsForValidator.scoreValid) {
                    console.log("functionsForValidator", scope.functionsForValidator.scoreValid);
                }
            }
        },
        controller: 'CurrentGameController',
        templateUrl: 'client/scoreKeeper/directives/currentGame/views/currentGameDir.ng.html'
    };
})
    .directive('decreaseBid', function() {
        return {
            restrict: 'A',
            link: function(scope, element) {
                element.bind('click', function() {
                    console.log("decrease bid ", scope.$parent.padding.teamBid);
                    // subtract one from bid

                    if(scope.$parent.padding.teamBid === undefined) {
                        scope.$parent.padding.teamBid = 0;
                    } else if(scope.$parent.padding.teamBid > 1) {
                        scope.$parent.padding.teamBid--;
                    } else {
                        console.log("Cannot bid less than 1");
                    }
                    scope.$apply();
                });
            }
        };
    })
    .directive('increaseBid', function() {
        return {
            restrict: 'A',
            link: function(scope, element) {
                element.bind('click', function() {
                    // subtract one from bid
                    console.log("increase bid ", scope.$parent.padding.teamBid);
                    if(scope.$parent.padding.teamBid === undefined) {
                        scope.$parent.padding.teamBid = 1;
                    } else if(scope.$parent.padding.teamBid <= 12) {
                        scope.$parent.padding.teamBid++;
                    } else {
                        console.log("Cannot bid more than 13");
                    }
                    scope.$apply();
                });
            }

        };
    });