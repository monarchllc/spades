
angular.module("socially").service("bidService",
    function (SpadesRulesService) {
        var that = this;

        //that.biddingErrors = [];
        console.log("what does spades rules look like? ", SpadesRulesService);

        that.isTeamBidValid = function(teamBid) {
            if(teamBid < SpadesRulesService.minBid || teamBid > SpadesRulesService.maxBid) {
                return false;
            }
            return true;
        };

        that.checkBids = function (team1Bid, team2Bid){
            var isBidValid = true;
            //that.biddingErrors = [];
            if(team1Bid  < 1) {
                isBidValid = false;
                //that.biddingErrors.push({violatingTeam: 1, violation:"You must bid at least 1 book"});
            }

            if(team2Bid  < 1) {
                isBidValid = false;
                //that.biddingErrors.push({violatingTeam: 2, violation:"You must bid at least 1 book"});
            }

            if(team1Bid < SpadesRulesService.minBid) {
                isBidValid = false;
                //that.biddingErrors.push({violatingTeam: 1, violation:"You must bid a minimum of "+ SpadesRulesService.minBid + " books"});
            }

            if(team2Bid < SpadesRulesService.minBid) {
                isBidValid = false;
                //that.biddingErrors.push({violatingTeam: 2, violation:"You must bid a minimum of "+ SpadesRulesService.minBid + " books"});
            }

            console.log("checking bids ", isBidValid, team1Bid, team2Bid);
            return isBidValid;
        };
        return that;
    });