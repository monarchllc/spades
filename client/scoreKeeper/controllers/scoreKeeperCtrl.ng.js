
angular.module("socially").controller("scoreKeeperCtrl",
    function ($scope, bidService, SpadesTeamFactory, $log) {

        // This may turn into it's own Service
        var history = [];

        $scope.team1 = new SpadesTeamFactory("US");

        $scope.team2 = new SpadesTeamFactory("THEM");

        $scope.runningTotal = 0;

        $scope.finalize = function() {
            // sanity check for valid bid even though the form is validated
            if(bidService.checkBids($scope.team1.currentBid, $scope.team2.currentBid)) {
                $scope.team1.isScoreFinalized = true;
                $scope.team2.isScoreFinalized = true;
            }
        };

        $scope.submitGameScore = function(team1Score, team2Score) {
            $scope.team1.score = team1Score;
            $scope.team2.score = team2Score;

            $scope.team1.calculateHandScore();
            $scope.team2.calculateHandScore();
        };

    });