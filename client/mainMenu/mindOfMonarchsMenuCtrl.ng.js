angular.module("socially")
    .controller("MindsOfMonarchsCtrl",
        function ($scope) {
            $scope.menuItems = [
                {title:'Scoreboard', author:"James Singleton", state:'scoreboard', description:'Spades Scoreboard web application'},
                {title:'P+W Wave', author: "Kiel Byrne", link:'http://pw-pulse.meteor.com', description:'App that serves as a central contact information for a company with several satellite offices'}];
        });