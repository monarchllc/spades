angular.module("socially")
    .controller("MainMenuController",
    function ($scope) {

        $scope.menuItems = [
            {title:'From the Minds of Monarchs', state:'mindsOfMonarchs', description:'Apps from our Developers'},
            {title:'Developer Bulletin Board', state:'blog',  description:'A spot for Developers to post various coding type things that they come across.'},
            {title:'Website Builder', link:"http://www.monarch-llc.com/playground/siteBuilder", description:'Allows anyone to build a one page site with bootstrap components.' }];
    });