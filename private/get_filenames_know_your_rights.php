<?php
// grab the files
$directoryName = dirname(__FILE__)."/rights/";
$files = scandir($directoryName);

// remove "." and ".." (and anything else you might not want)
$output = array();
foreach ($files as $file)
  if (!in_array($file, array(".", "..")))
    $output[] = $file;

// out we go
header("Content-type: application/json");
header("Access-Control-Allow-Origin: *");
echo json_encode($output);
?>